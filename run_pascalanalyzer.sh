#!/bin/bash 
#SBATCH --job-name=PascalAnalyzer 
#SBATCH --time=7-0:0
#SBATCH --cpus-per-task=32
#SBATCH --hint=compute_bound
#SBATCH --exclusive
#SBATCH --qos=qos2

source env.sh

package_names=(
  #  "blackscholes"
  #  "bodytrack"
    "facesim" 
  #  "ferret"  
  #  "fluidanimate"
  #  "freqmine" 
  #  #"raytrace" It's not have pascal.runconf
  # "swaptions"
  #  "vips"
  #  "x264"
  #  "canneal"
    #"dedup" It's not compile with pascalops.h
    #"streamcluster" It's not have input yet
);


for package_name in ${package_names[@]}; do
    parsecmgmt -a run -p $package_name -c gcc-pascal -i pascal -n 32;
done;

