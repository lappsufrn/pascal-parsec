#ifndef _ENCODER_H_
#define _ENCODER_H_ 1

#include "dedupdef.h"

void Encode(config_t *conf);

#endif /* !_ENCODER_H_ */
