# Modificações feitas no Parsec para inserir o Pascal


Adicionar o pascal no Parsec foi adicionar os seguintes arquivos e pastas:

- [pkgs/libs/pascal-releases](pkgs/libs/pascal-releases/)
- [config/packages/parsec.pascal-releases.pkgconf](config/packages/parsec.pascal-releases.pkgconf)
- [config/gcc-pascal.bldconf](config/gcc-pascal.bldconf)

e para cada benchmark: \<benchmark\>/parsec/gcc-pascal.bldconf

## Pascal como um pacote do parsec

O parsec divide a sua aplicação em pacotes, portanto adicionar o Pascal
ao Parsec é adicionar um novo pacote. Um pacote do parsec possui um arquivo de
configuração presente em [config/packages](config/packages) e uma subpasta dentro de [pkgs](pkgs) com o
mesmo nome. No nosso caso os arquivos são: [parsec.pascal-releases.pkgconf](config/packages/parsec.pascal-releases.pkgconf) e  [pkgs/libs/pascal-releases](pkgs/libs/pascal-releases).  No arquivo [parsec.pascal-releases.pkgconf](config/packages/parsec.pascal-releases.pkgconf) encontram-se metadados do pacote, como quem é o contribuidor do pacote, como citar o contribuidor, uma descrição do pacote e etc.  Na subpasta dentro do [pkgs](pkgs) (pkgs/libs/pascal-releases) o parsec espera encontrar uma subpasta chamada parsec ([/pkgs/libs/pascal-releases/parsec](pkgs/libs/pascal-releases/parsec)) que contém scripts do tipo .bldconf (build config). Este tipo de script fornece as informações necessárias para compilar o pacote. Uma informação importante sobre o parsec é que ele decidiu considerar o nome do arquivo como também parte da informação de compilação. Por exemplo, um script chamado gcc-pthreads.bldconf será executado quando o usuário informar a flag -c gcc-pthreads.

## Compilando pacotes com o pascal

Adicionar um pacote é o suficiente para compila-lo, mas para utilizar o pacote em conjunto aos benchmarks foi criado o arquivo [config/gcc-pascal.bldconf](config/gcc-pascal.bldconf), esse arquivo adiciona um novo parâmetro chamado gcc-pascal, na flag -c e configura as variáveis de ambiente para linkar a biblioteca dinâmica do pascal-release aos benchmarks. Cada benchmark suportado também tem seu respectivo arquivo gcc-pascal.bldconf na sua pasta parsec, o pacote pascal-releases também tem esse arquivo. Durante a compilação o parsec buscar encontrar ou um arquivo makefile dentro da pasta do pacote ou um shell script configure. Como o pascal-release é uma biblioteca previamente compilada, o makefile presente no pacote não faz nada, existindo apenas para não apresentar erros durante a compilação. O parsec cria variáveis de ambiente para adicionar flags ao compilador gcc. Além do arquivo [config/gcc-pascal.bldconf](config/gcc-pascal.bldconf) passar as flags para linkar biblioteca dinâmica, ele cria um novo define chamado: __ENABLE_PASCAL_HOOKS__, que foi bastante utilizado para modificações feitas nos códigos dos benchmarks. Para compilar um benchmark com o pascal basta executar o comando:

```shell
parsecmgmt -a build -p <package> -c gcc-pascal
```






[parsec.pascal-releases.pkgconf]:config/packages/parsec.pascal-releases.pkgconf
[pkgs/libs/pascal-releases]:pkgs/libs/pascal-releases/

[config/gcc-pascal.bldconf]:config/gcc-pascal.bldconf
